<?php

namespace App\Http\Controllers;

use App\Models\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AlbumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * List of all albums
     * 
     * @return array
     */
    public function index()
    {
        $albums = Album::all()->toArray();
        return array_reverse($albums);
    }

    /**
     * Store album
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $album = new Album($request->all());
        $album->save();
        Log::info("Album \"{$request->name}\" (id: {$request->id}) was created.");
        return response()->json('Album created!');
    }

    /**
     * Show album information
     * 
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $album = Album::find($id);
        return response()->json($album);
    }

    /**
     * Update album information
     * 
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $album = Album::find($id);
        $album->update($request->all());
        Log::info("Album \"{$request->name}\" (id: {$request->id}) was updated.");
        return response()->json('Album updated!');
    }

    /**
     * Delete album information
     * 
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $album = Album::find($id);
        $album->delete();
        Log::info("Album \"{$album->name}\" (id: {$id}) was deleted.");
        return response()->json('Album deleted!');
    }
}
