<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\LastFmController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->group(function () {
    Route::resource('albums', AlbumController::class);
});

Route::middleware('api')->group(function() {
    Route::get('/lastfm/artist={artist}', [LastFmController::class, 'searchArtists']);
    Route::get('/lastfm/album={album}', [LastFmController::class, 'searchAlbums']);
    Route::get('/lastfm/artist={artist}/album={album}', [LastFmController::class, 'getArtistAlbum']);
    Route::get('/lastfm/albums/top/artist={artist}', [LastFmController::class, 'getArtistTopAlbums']);
});
