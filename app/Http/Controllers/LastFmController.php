<?php

namespace App\Http\Controllers;

use App\Services\LastFmService;

class LastFmController extends Controller
{
    private $lastFm;

    public function __construct()
    {
        $this->lastFm = new LastFmService(env('LASTFM_API_KEY'));
    }

    public function searchArtists($artist)
    {
        return $this->lastFm->searchArtists($artist);
    }

    public function searchAlbums($album)
    {
        return $this->lastFm->searchAlbums($album);
    }

    public function getArtistTopAlbums($artist)
    {
        return $this->lastFm->getArtistTopAlbums($artist);
    }

    public function getArtistAlbum($artist, $album)
    {
        return $this->lastFm->getArtistAlbum($artist, $album);
    }
}
