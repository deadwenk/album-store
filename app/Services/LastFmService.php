<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LastFmService
{
    private $uri;
    private $version;
    private $format;
    private $apiKey;

    /**
     * LastFmController constructor
     * 
     * @param string $apiKey API key
     * @param string $uri address
     * @param string $version API version
     * @param string $format returning format, possible values: 'json', 'xml'
     */
    public function __construct($apiKey, $uri = 'http://ws.audioscrobbler.com', $version = '2.0', $format = 'json')
    {
        if (empty($apiKey)) {
            throw new InvalidArgumentException("LastFM Api key is empty");
        }
        if (!filter_var($uri, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException("Invalid uri argument: \"{$uri}\"");
        }
        if (!in_array($format, ['json', 'xml'])) {
            throw new InvalidArgumentException("Unknown format value: \"{$format}\"");
        }

        $this->apiKey = $apiKey;
        $this->uri = $uri;
        $this->version = $version;
        $this->format = $format;
    }

    /**
     * Preparing URL to get album/artist information
     * 
     * @param string $method required method: 'album.*', 'artist.*' and so one
     * @param array<int, string> $params URL arguments
     * @return string
     */
    private function cookUrl($method, $params = [])
    {
        $url = $this->uri . "/{$this->version}/?";
        $params['method'] = $method;
        $params['format'] = $this->format;
        $params['api_key'] = $this->apiKey;

        foreach ($params as $key => $value) {
            $url .= "{$key}={$value}&";
        }

        return substr($url, 0, -1);
    }

    /**
     * Collecting data from fetch
     * 
     * @param array $data hashstack
     * @param array<int, string> $keys filtering columns
     * @return array
     */
    private function collect($data, $keys = [])
    {
        $id = 0;
        return array_map(function ($el) use ($keys, $id) {
            // if ($el['name'] === '(null)') return;
            $data = array_intersect_key($el, array_flip($keys));
            $data['id'] = $id++;
            return $data;
        }, $data);
    }

    /**
     * Search the artist
     * 
     * @param string $artist sought artist
     * @return array artist information
     */
    public function searchArtists($artist)
    {
        $url = $this->cookUrl('artist.search', [
            'artist' => $artist
        ]);

        $response = Http::get($url)->json();
        if (isset($response['error'])) {
            return [];
        }

        return $this->collect($response['results']['artistmatches']['artist'], ['name']);
    }

    /**
     * Search the album
     * 
     * @param string $album sought album
     * @return array album information
     */
    public function searchAlbums($album)
    {
        $url = $this->cookUrl('album.search', [
            'album' => $album
        ]);

        $response = Http::get($url)->json();
        if (isset($response['error'])) {
            return [];
        }

        return $this->collect($response['results']['albummatches']['album'], ['name', 'artist', 'image']);
    }

    /**
     * Getting artist top album list
     * 
     * @param string $artist sought artist
     * @return array
     */
    public function getArtistTopAlbums($artist)
    {
        $url = $this->cookUrl('artist.gettopalbums', [
            'artist' => $artist
        ]);

        $response = Http::get($url)->json();
        if (isset($response['error'])) {
            return [];
        }

        return $this->collect($response['topalbums']['album'], ['name']);
    }

    /**
     * Getting artist album
     * 
     * @param string $artist sought artist
     * @param string $album sought album
     * @return array
     * @throws NotFoundHttpException
     */
    public function getArtistAlbum($artist, $album)
    {
        $url = $this->cookUrl('album.getinfo', [
            'artist' => $artist,
            'album' => $album
        ]);

        $response = Http::get($url)->json();

        if (isset($response['error'])) {
            throw new NotFoundHttpException("Artist \"$artist\" album \"$album\" not found");
        }

        return array_intersect_key($response['album'], array_flip(['name', 'artist', 'image', 'wiki']));
    }
}